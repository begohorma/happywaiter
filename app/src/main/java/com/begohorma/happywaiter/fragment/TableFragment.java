package com.begohorma.happywaiter.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.begohorma.happywaiter.R;
import com.begohorma.happywaiter.activity.MenuActivity;
import com.begohorma.happywaiter.adapter.DishRecyclerViewAdapter;
import com.begohorma.happywaiter.model.Dish;
import com.begohorma.happywaiter.model.Restaurant;
import com.begohorma.happywaiter.model.Table;

import java.util.LinkedList;


public class TableFragment extends Fragment{

    private static final String ARG_TABLE = "table";
    private static final int REQUEST_DISH_INDEX = 0;


    private Table mTable;
    private View mRoot;
    private RecyclerView mList;

    //Crear instancia del fragment pasandole como argumento el indice de la mesa seleccionada
    public  static TableFragment newInstance(Table table){
        TableFragment fragment = new TableFragment();

        Bundle arguments = new Bundle();
        arguments.putSerializable(ARG_TABLE,table);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Indicar que tiene menu
        setHasOptionsMenu(true);

        //recoger los argumentos
        if(getArguments() != null){
            mTable = (Table) getArguments().getSerializable(ARG_TABLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);

        final LinkedList<Dish> dishes = mTable.getDishes();


            mRoot= inflater.inflate(R.layout.fragment_dish,container,false);

            //Acceder al RecyclerView
            mList = (RecyclerView) mRoot.findViewById(R.id.dish_list);
            //Indicar el Layout al RecyclerView
            mList.setLayoutManager(new GridLayoutManager(getActivity(),getResources().getInteger(R.integer.recycler_columns)));

            //actualizar datos
            updateDish();


        return mRoot;


    }

    private void updateDish(){

       final LinkedList<Dish> dishes = mTable.getDishes();

            //Asignar adapter al RecyclerView
            DishRecyclerViewAdapter adapter = new DishRecyclerViewAdapter(dishes);

        //TODO cuando se seleecione un plato mostrar vista detalle
//            //suscribirse a las pulsaciones de tarjeta del Recicler
//            adapter.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int positon = mList.getChildAdapterPosition(v);
//                    //se llamará a la vista detalle del plato
//                }
//            });

            mList.setAdapter(adapter);
    }

//menu para añadir los platos


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_dishes,menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean superReturn= super.onOptionsItemSelected(item);
    if(item.getItemId() == R.id.menu_show_dishes){
        Intent menuIntent = new Intent(getActivity(), MenuActivity.class);
        //La actividad tienee que devolver el plato seleccionado
        startActivityForResult(menuIntent,REQUEST_DISH_INDEX);
        return true;
    }
    if(item.getItemId() == R.id.menu_price){
        calculateTotal();
    }

        return superReturn;
    }

    private void calculateTotal() {
        TextView total = (TextView) mRoot.findViewById(R.id.table_total);
        total.setText(getString(R.string.table_total_format,mTable.getTotal()));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_DISH_INDEX){
            //Se ha seleccionado un plato para añadir en la mesa actual
            Snackbar.make(getView(), "Se ha seleccionado un plato", Snackbar.LENGTH_LONG).show();
            if(resultCode == Activity.RESULT_OK){
                int dishSelected = data.getIntExtra(MenuActivity.DISH_SELECTED_INDEX,0);
                Restaurant restaurant = Restaurant.getInstance();
                 LinkedList<Dish> dishes = restaurant.getMenu();
                Dish selectedDish = dishes.get(dishSelected);

                LinkedList<Dish> tableDishes = mTable.getDishes();
                if (tableDishes == null){
                    tableDishes = new LinkedList<Dish>();
                }
                tableDishes.add(selectedDish);
                mTable.setDishes(tableDishes);
                updateDish();

            }
        }
    }
}
