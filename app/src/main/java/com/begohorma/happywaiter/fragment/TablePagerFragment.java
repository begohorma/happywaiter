package com.begohorma.happywaiter.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.begohorma.happywaiter.R;
import com.begohorma.happywaiter.adapter.TablePagerAdapter;
import com.begohorma.happywaiter.model.Restaurant;

public class TablePagerFragment  extends Fragment{

    private static final String ARG_INITIAL_TABLE_INDEX = "ARG_INITIAL_TABLE_INDEX";

    private ViewPager mPager;
    private Restaurant mRestaurant;
    private int mInitialTableIndex = 0;

    //Crear instancia del fragment pasandole como argumento el indice de la mesa seleccionada
    public  static TablePagerFragment newInstance(int initialTableIndex){
        TablePagerFragment fragment = new TablePagerFragment();

        Bundle arguments = new Bundle();
        arguments.putInt(ARG_INITIAL_TABLE_INDEX,initialTableIndex);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //indicar que hay menu
        setHasOptionsMenu(true);

        //Si hay argumentos recoger el valor del argumento. valor por defecto 0
        if(getArguments() != null){
            mInitialTableIndex= getArguments().getInt(ARG_INITIAL_TABLE_INDEX,0);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);

        //inflar el layout para crear la vista
        View root= inflater.inflate(R.layout.fragment_table_pager,container,false);

        //aceder al pager
        mPager = (ViewPager) root.findViewById(R.id.table_view_pager);
        mRestaurant = Restaurant.getInstance();

        //Crear y asignar el adapter
       TablePagerAdapter adapter = new TablePagerAdapter(getFragmentManager(),mRestaurant);
        mPager.setAdapter(adapter);

    // Añadir listener para saber cuando se cambia la página del pager
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
               updateTableInfo(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        moveToTable(mInitialTableIndex);
        updateTableInfo(mInitialTableIndex);
        return root;
    }


    public void moveToTable(int tableIndex){
        mPager.setCurrentItem(tableIndex);
    }

    private void updateTableInfo(int position){
        String tableName = mRestaurant.getTable(position).getName();

        if(getActivity() instanceof AppCompatActivity){
            AppCompatActivity parentActivity = (AppCompatActivity) getActivity();
            ActionBar toolbar = parentActivity.getSupportActionBar();
            if(toolbar != null){
                toolbar.setTitle(tableName);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_pager,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean superReturn = super.onOptionsItemSelected(item);
        getActivity().invalidateOptionsMenu();
        if(item.getItemId()== R.id.previous){
            //mover el pager hacia atrás
            moveToTable(mPager.getCurrentItem() -1);
            return true;
        }
        else if (item.getItemId()== R.id.next){
            //mover pager hacia adelante
            moveToTable(mPager.getCurrentItem() +1);

            return true;
        }

        return superReturn;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem menuPrev = menu.findItem(R.id.previous);
        MenuItem menuNext = menu.findItem(R.id.next);

//        //habilitar el menú anterior si el elemento seleccionado es mayor al primero
//        menuPrev.setEnabled(mPager.getCurrentItem() > 0);
//        //habilitar el menu siguiente si el elemento actual no es el último
//        menuNext.setEnabled(mPager.getCurrentItem() < mRestaurant.getCount() -1);

        if (mPager.getCurrentItem() > 0) {
            // Puedo ir hacia atrás
            menuPrev.setEnabled(true);
        }
        else {
            menuPrev.setEnabled(false);
        }

        if (mPager.getCurrentItem() < mRestaurant.getCount() - 1) {
            // Puedo ir hacia delante
            menuNext.setEnabled(true);
        }
        else {
            menuNext.setEnabled(false);
        }
    }
}

