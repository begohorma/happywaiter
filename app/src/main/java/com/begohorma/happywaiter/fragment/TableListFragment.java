package com.begohorma.happywaiter.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.begohorma.happywaiter.R;
import com.begohorma.happywaiter.model.Table;

import java.util.LinkedList;

public class TableListFragment extends Fragment {

    //recibe como argumento la lista de mesas
    private final static String ARG_TABLES = "tables";

    protected LinkedList<Table> mTables;
    protected OnTableSelectedListener mOnTableSelectedListener;

    public static TableListFragment newInstance(LinkedList<Table> tables){
        TableListFragment fragment = new TableListFragment();

        Bundle arguments = new Bundle();
        arguments.putSerializable(ARG_TABLES,tables);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Obtener la lista de mesas de los argumentos
        if(getArguments() !=null){
            mTables = (LinkedList<Table>) getArguments().getSerializable(ARG_TABLES);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        //Crear la vista inflando el layout
        View root = inflater.inflate(R.layout.fragment_table_list,container,false);

        //Accedeer al listView de la vista
        ListView list = (ListView) root.findViewById(R.id.table_list);

        //Crear adapter para la lista de mesas. Como es una lista corta vale un arrayAdapter
        ArrayAdapter<Table> adapter =  new ArrayAdapter<Table>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                mTables
        );

        //Asignar el adapter al listView
        list.setAdapter(adapter);

        //Asignar listener al listView para saber cuando se ha pulsado una fila
        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Si hay algun listener avidar que se ha seleccionado una mesa
               if(mOnTableSelectedListener != null){
                   Table selectedTable = mTables.get(position);

                   //Avisar al listener
                   mOnTableSelectedListener.onTableSelected(selectedTable,position);
               }
            }
        });


        return root;


    }

    //Métodos para añadir y quitar a la actividad a la que hay que avisar de cambios

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(getActivity() instanceof OnTableSelectedListener){
            mOnTableSelectedListener = (OnTableSelectedListener) getActivity();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(getActivity() instanceof OnTableSelectedListener){
            mOnTableSelectedListener = (OnTableSelectedListener) getActivity();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnTableSelectedListener = null;
    }

    //Interfaz que debe implementar quien quiera saber los que le pasa al fragment (ResyaurantActivity)
    public interface OnTableSelectedListener{
        void onTableSelected(Table table, int position);
    }
}
