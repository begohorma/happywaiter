package com.begohorma.happywaiter.activity;


import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.begohorma.happywaiter.R;
import com.begohorma.happywaiter.fragment.TablePagerFragment;

public class TablePagerActivity extends AppCompatActivity{

    public static final String EXTRA_TABLE_INDEX = "EXTRA_TABLE_INDEX";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_pager);

        //Usar toolbar personalizada
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //Recibir el índice de la mesa a mostrar
        int tableIndex = getIntent().getIntExtra(EXTRA_TABLE_INDEX,0);

        //Añadir fragment a la jerarquía si no está añadido ya
        FragmentManager fm = getFragmentManager();
        if(fm.findFragmentById(R.id.table_view_pager_fragment) == null){
            TablePagerFragment fragment = TablePagerFragment.newInstance(tableIndex);
            fm.beginTransaction()
                    .add(R.id.table_view_pager_fragment,fragment)
                    .commit();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean superValue = super.onOptionsItemSelected(item);

        if (item.getItemId() == android.R.id.home) {
            // Han pulsado la flecha de back de la ActionBar, finalizamos la actividad
            finish();
            return true;
        }

        return superValue;
    }
}
