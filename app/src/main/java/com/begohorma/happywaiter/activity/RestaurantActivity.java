package com.begohorma.happywaiter.activity;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.begohorma.happywaiter.R;
import com.begohorma.happywaiter.fragment.TableListFragment;
import com.begohorma.happywaiter.fragment.TablePagerFragment;
import com.begohorma.happywaiter.model.Restaurant;
import com.begohorma.happywaiter.model.Table;

public class RestaurantActivity extends AppCompatActivity  implements TableListFragment.OnTableSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        //Cargar fragment

        FragmentManager fm = getFragmentManager();

        if(findViewById(R.id.table_list_fragment)!=null){
            if(fm.findFragmentById(R.id.table_list_fragment) == null){
                //No está añadido el fragment a la jerarquía
                Restaurant restaurant = Restaurant.getInstance();

               TableListFragment fragment = TableListFragment.newInstance(restaurant.getTables());

                fm.beginTransaction()
                        .add(R.id.table_list_fragment,fragment)
                        .commit();
            }
        }
        //Tableta
        if(findViewById(R.id.table_view_pager_fragment)!= null){
            if (fm.findFragmentById(R.id.table_view_pager_fragment)== null){
                fm.beginTransaction()
                        .add(R.id.table_view_pager_fragment, TablePagerFragment.newInstance(0))
                        .commit();
            }
        }
    }

    @Override
    public void onTableSelected(Table table, int position) {
        //Identificar los fragments cargados en la interfaz

        FragmentManager fm= getFragmentManager();
        TablePagerFragment tablePagerFragment = (TablePagerFragment) fm.findFragmentById(R.id.table_view_pager_fragment);

        if (tablePagerFragment != null){
         //Hay un pager. Indicarla ciudad a mostra
            tablePagerFragment.moveToTable(position);
        }
        else{
            //No hay un pager. Lanzar la actividad TablePagerActivity
            Intent intent= new Intent(this, TablePagerActivity.class);
            intent.putExtra(TablePagerActivity.EXTRA_TABLE_INDEX,position);
            startActivity(intent);
        }
    }
}
