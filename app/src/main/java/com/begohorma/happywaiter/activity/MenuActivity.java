package com.begohorma.happywaiter.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.begohorma.happywaiter.R;
import com.begohorma.happywaiter.adapter.DishRecyclerViewAdapter;
import com.begohorma.happywaiter.model.Dish;
import com.begohorma.happywaiter.model.Restaurant;

import java.util.LinkedList;

public class MenuActivity  extends AppCompatActivity{

    public static final String DISH_SELECTED_INDEX = "DISH_SELECTED_INDEX";

    private Restaurant mRestaurant;
    private LinkedList<Dish> mDishes;
    private RecyclerView mList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu);
        Restaurant restaurant = Restaurant.getInstance();
        mDishes= restaurant.getMenu();
        //View root= inflater.inflate(R.layout.fragment_dish,container,false);

        //Acceder al RecyclerView
        mList = (RecyclerView) findViewById(R.id.dish_list);
        //Indicar el Layout al RecyclerView
        mList.setLayoutManager(new GridLayoutManager(this,getResources().getInteger(R.integer.recycler_columns)));

        //Asignar adapter al RecyclerView
        DishRecyclerViewAdapter adapter = new DishRecyclerViewAdapter(mDishes);


        //suscribirse a las pulsaciones de tarjeta del Recicler
            adapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int positon = mList.getChildAdapterPosition(v);
                    //devolver el indice del plato seleccionado
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(DISH_SELECTED_INDEX,positon);
                    setResult(RESULT_OK,returnIntent);
                    finish();
                }
            });


        mList.setAdapter(adapter);


    }


}
