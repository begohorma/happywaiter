package com.begohorma.happywaiter.adapter;


import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.begohorma.happywaiter.R;
import com.begohorma.happywaiter.model.Dish;

import java.util.LinkedList;
import java.util.Objects;

public class DishRecyclerViewAdapter extends RecyclerView.Adapter<DishRecyclerViewAdapter.DishViewHolder> {

    private LinkedList<Dish> mDishes;
    private View.OnClickListener mOnClickListener;


    public DishRecyclerViewAdapter(LinkedList<Dish> dishes) {
        super();

        mDishes = dishes;
    }


    public void setOnClickListener(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public DishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_dish,parent,false);
        view.setOnClickListener(mOnClickListener);
        return new DishViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(DishViewHolder holder, int position) {
        holder.bindDish(mDishes.get(position));
    }


    @Override
    public int getItemCount() {
        return mDishes.size();
    }


    //ViewHolder

    protected  class DishViewHolder extends RecyclerView.ViewHolder{

        private View mRoot;
        //Los elementos de la vista son las propiedades del Viewholder

        private final ImageView mDishImage;
        private final TextView mDishName;
        private final TextView mDishType;
        private final TextView mDishPrice;
        private ViewGroup mDishAllergens=null;

        public DishViewHolder(View itemView) {
            super(itemView);

            //Acceder a la vistas
            mDishImage = (ImageView) itemView.findViewById(R.id.dish_image);
            mDishName = (TextView) itemView.findViewById(R.id.dish_name);
            mDishType = (TextView) itemView.findViewById(R.id.dish_type);
            mDishPrice= (TextView) itemView.findViewById(R.id.dish_price);
            mDishAllergens = (ViewGroup) itemView.findViewById(R.id.allergens_container);

        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        public void bindDish (Dish dish){
            Context context = mDishImage.getContext();

            //Actualizar la vista con el modelo
            mDishImage.setImageResource(dish.getImage());
            mDishName.setText(dish.getName());
            mDishType.setText(context.getString(R.string.dish_type_format,dish.getType()));
            mDishPrice.setText(context.getString(R.string.dish_price_format,dish.getPrice()));
            //TODO eliminar el espacio por encima de la imagen de los alergenos
            mDishAllergens.removeAllViews();
            for(int i=0; i< dish.getAllergenesCount();i++){
                if(!Objects.equals(dish.getAllergen(i), "ninguno")){
                    ImageView allergenImage = new ImageView(context);
                    allergenImage.setImageResource(getAllergenImageResource(dish.getAllergen(i)));
                    allergenImage.setLayoutParams(new ViewGroup.LayoutParams(300,200));
                    mDishAllergens.addView(allergenImage);
                }

            }

        }
    }
    private int getAllergenImageResource(String allergenName) {

        int imageResource = R.drawable.alubias_sacramentos;

        //TODO: mejorarlo para no tener que poner los nonmbres a mano
        switch ( allergenName){
            case "altramuces":
                imageResource= R.drawable.altramuces;
                break;
            case "apio":
                imageResource= R.drawable.apio;
                break;
            case "cacahuetes":
                imageResource= R.drawable.cacahuetes;
                break;
            case "crustaceos":
                imageResource= R.drawable.crustaceos;
                break;
            case "dioxidoazufre_sulfitos":
                imageResource= R.drawable.dioxidoazufre_sulfitos;
                break;
            case "frutos con cascaras":
                imageResource= R.drawable.frutos_de_cascara;
                break;
            case "gluten":
                imageResource= R.drawable.gluten;
                break;
            case "granos sesamo":
                imageResource= R.drawable.granos_sesamo;
                break;
            case "huevos":
                imageResource= R.drawable.huevos;
                break;
            case "lacteos":
                imageResource= R.drawable.lacteos;
                break;
            case "moluscos":
                imageResource= R.drawable.moluscos;
                break;
            case "mostaza":
                imageResource= R.drawable.mostaza;
                break;
            case "pescado":
                imageResource= R.drawable.pescado;
                break;
            case "Pescado":
                imageResource= R.drawable.pescado;
                break;
            case "soja":
                imageResource= R.drawable.soja;
                break;
        }
        return imageResource;
    }
}
