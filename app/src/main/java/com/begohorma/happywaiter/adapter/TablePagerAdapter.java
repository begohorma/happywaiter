package com.begohorma.happywaiter.adapter;


import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.begohorma.happywaiter.fragment.TableFragment;
import com.begohorma.happywaiter.model.Restaurant;
import com.begohorma.happywaiter.model.Table;

public class TablePagerAdapter extends FragmentPagerAdapter {

    private Restaurant mRestaurant;

    public TablePagerAdapter(FragmentManager fm, Restaurant restaurant) {
        super(fm);
        mRestaurant = restaurant;
    }

    @Override
    public Fragment getItem(int position) {
        TableFragment fragment = TableFragment.newInstance(mRestaurant.getTable(position));
        return fragment;
    }

    @Override
    public int getCount() {
        return mRestaurant.getCount();
    }

    //Mostrar nombre Mesa en el Pager
    @Override
    public CharSequence getPageTitle(int position) {
        super.getPageTitle(position);
        Table table = mRestaurant.getTable(position);
        String tableName= table.getName();
        return tableName;
    }
}
