package com.begohorma.happywaiter.model;


import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Dish implements Serializable {

    private String mId = null;
    private String mName = null;
    private String mType = null;
    private float mPrice ;
    private String mDescription = null;
    private int mImage; //como son recursos locales guardo el valor de la referencia
    private String mVariants = null;
    private List<String> mAllergens = new LinkedList<>();

    public Dish(String id, String name, String type, float price, String description, int image) {
        mId = id;
        mName = name;
        mType = type;
        mPrice = price;
        mDescription = description;
        mImage = image;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        mPrice = price;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public int getImage() {
        return mImage;
    }

    public void setImage(int image) {
        mImage = image;
    }

    public String getVariants() {
        return mVariants;
    }

    public void setVariants(String variants) {
        mVariants = variants;
    }

    public void addAllergen(String allergen){
        mAllergens.add(allergen);
    }

    public int getAllergenesCount(){
        return mAllergens.size();
    }

    public String getAllergen(int index){
        return mAllergens.get(index);
    }
}


