package com.begohorma.happywaiter.model;


import java.io.Serializable;
import java.util.LinkedList;

public class Table  implements Serializable{

    private String mName;
    private LinkedList<Dish> mDishes;

    public Table(String name, LinkedList<Dish> dishes) {
        mName = name;
        mDishes = dishes;
    }

    public Table(String name) {
        this(name,null);
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public LinkedList<Dish> getDishes() {
        return mDishes;
    }

    public void setDishes(LinkedList<Dish> dishes) {
        mDishes = dishes;
    }

    //sobrescribir método toString para que devuelva el nombre de la mesa

    @Override
    public String toString() {
        return getName();
    }

    public float getTotal(){
        float tot = 0;

        LinkedList<Dish> list = this.getDishes();

        for(int i =0; i < list.size();i++){
            tot += list.get(i).getPrice();
        }
        return tot;
    }
}


