package com.begohorma.happywaiter.model;


        import android.os.AsyncTask;

import com.begohorma.happywaiter.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;

public class Restaurant {

    //Usar singleton para mantener el estado de la app
    private static Restaurant mInstance;

    private static String jsonUrl = "http://www.mocky.io/v2/593c46f3100000c41ac47810";
    private static final int NUMBER_OF_TABLES = 3;// TODO: hacer un settings que pida el número de mesas del restaurante

    private LinkedList<Table> mTables;
    private LinkedList<Dish>mMenu;

    public static Restaurant getInstance(){
        if(mInstance == null){
            mInstance= new Restaurant();
        }
        return mInstance;
    }

    public Restaurant() {
        mTables = new LinkedList<>();
        mMenu = new LinkedList<>();

        //Crear mesas.
       for(int i =0; i < NUMBER_OF_TABLES; i++){
           String name = "Mesa " + (i+1);
           mTables.add(new Table(name));
           mTables.get(i).setDishes(new LinkedList<Dish>());
       }


        //Datos de prueba
//        LinkedList<Dish> dis = new LinkedList<>();
//       Dish d1 =new Dish("0","Plato de prueba","primero",12,"descripción del plato de prueba",R.drawable.croquetas);
//        d1.addAllergen("gluten");
//        d1.addAllergen("lacteos");
//        Dish d2 =new Dish("17","Plato de prueba2","segundo",14.25f,"descripción del plato de prueba 2",R.drawable.bacalao_pilpil);
//        d2.addAllergen("pescado");
//        d2.addAllergen("lacteos");
//        d2.addAllergen("crustaceos");
//        dis.add(d1);
//        dis.add(d2);
//        mTables.get(0).setDishes(dis);



        try{


            if (mMenu.size()== 0){
                //Descargar los platos en segundo plano
                AsyncTask<Void,Integer,LinkedList<Dish>>
                        dishesDownloader = new AsyncTask<Void, Integer, LinkedList<Dish>>() {
                    @Override
                    protected LinkedList<Dish> doInBackground(Void... params) {
                        return downloadDishes();
                    }

                    @Override
                    protected void onPostExecute(LinkedList<Dish> dishes) {
                        super.onPostExecute(dishes);
                        if (dishes !=null){
                            setMenu(dishes);
                        }
                    }
                };

                dishesDownloader.execute();

                return;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private LinkedList<Dish> downloadDishes() {

        URL url= null;
        InputStream input = null;

        try{
            url = new URL(String.format(jsonUrl));
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.connect();
            byte data[] = new byte[1024];
            int downloadedBytes;
            input= con.getInputStream();
            StringBuilder sb= new StringBuilder();
            while((downloadedBytes = input.read(data)) != -1){
                sb.append(new String(data,0,downloadedBytes));
            }

            //Analizar datos para psasrlos de JSON a objetos

            JSONArray list = new JSONArray(sb.toString());

            //Lista de platos
            LinkedList<Dish> dishes = new LinkedList<>();

            for(int i=0; i< list.length();i++){
                JSONObject jsonDish = list.getJSONObject(i);

                String id = jsonDish.getString("id");
                String name = jsonDish.getString("name");
                String type = jsonDish.getString("type");
                float price = (float) jsonDish.getDouble("price");
                String description = jsonDish.getString("description");
                String imageName = jsonDish.getString("image");

                //obtener el valor del drawable de la imagen
                int image = getImageResource(imageName);
                Dish dish = new Dish(id,name,type,price,description,image);

                JSONArray  jsonAllergens = jsonDish.getJSONArray("allergens");
                for (int j =0; j < jsonAllergens.length();j++) {

                    dish.addAllergen(jsonAllergens.getJSONObject(j).getString("allergen"));
                }
                dishes.add(dish);
            }

            return dishes;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    private int getImageResource(String imageName) {

        int imageResource = R.drawable.alubias_sacramentos;
        //TODO: mejorarlo para no tener que poner los nonmbres a mano
        switch (imageName){
            case "alubias_sacramentos.jpeg":
                imageResource= R.drawable.alubias_sacramentos;
                break;
            case "ensalada_mixta.jpeg":
                imageResource= R.drawable.ensalada_mixta;
                break;
            case "risotto_setas.jpeg":
                imageResource= R.drawable.risotto_setas;
                break;
            case "croquetas.jpeg":
                imageResource= R.drawable.croquetas;
                break;
            case "sopa_pescado.jpeg":
                imageResource= R.drawable.sopa_pescado;
                break;
            case "lasana_setas.jpeg":
                imageResource= R.drawable.lasana_setas;
                break;
            case "bacalao_pilpil.jpeg":
                imageResource= R.drawable.bacalao_pilpil;
                break;
            case "escalope_patatas.jpeg":
                imageResource= R.drawable.escalope_patatas;
                break;
            case "presa_iberica.jpeg":
                imageResource= R.drawable.presa_iberica;
                break;
            case "pollo_salsa.jpeg":
                imageResource= R.drawable.pollo_salsa;
                break;
            case "huevos_rotos_jamon.jpeg":
                imageResource= R.drawable.huevos_rotos_jamon;
                break;
            case "gallo_horno.jpeg":
                imageResource= R.drawable.gallo_horno;
                break;
            case "pina_natural.jpeg":
                imageResource= R.drawable.pina_natural;
                break;
            case "nueces_membrillo.jpeg":
                imageResource= R.drawable.nueces_membrillo;
                break;
            case "natillas_caseras.jpeg":
                imageResource= R.drawable.natillas_caseras;
                break;
            case "flan.jpeg":
                imageResource= R.drawable.flan;
                break;
        }
        return imageResource;
    }

    public LinkedList<Table> getTables() {
        return mTables;
    }

    public void setTables(LinkedList<Table> tables) {
        mTables = tables;
    }

    public Table getTable(int index){
        return mTables.get(index);
    }

    public int getCount(){
        return mTables.size();
    }

    public LinkedList<Dish> getMenu() {
        return mMenu;
    }

    public void setMenu(LinkedList<Dish> menu) {
        mMenu = menu;
    }


}

